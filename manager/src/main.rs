#[path = "../../libs/reader.rs"]
mod reader;

#[path = "../libs/auth.rs"]
mod auth;

#[path = "../libs/dashboard.rs"]
mod dashboard;

#[path = "../libs/healthcheck.rs"]
mod healthcheck;

//

fn main() {
  let api_port = reader::get_env_var("API_PORT", "8080");
  println!("API port: {}", api_port);

  let api_worker_timeout = reader::get_env_var("API_WORKER_TIMEOUT_MINUTES", "15");
  println!("API worker timeout (minutes): {}", api_worker_timeout);

  println!("\nStarting services...");
  // TODO: start cron tasks
}

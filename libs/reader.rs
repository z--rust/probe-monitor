use std::env;

pub fn get_env_var(var_name: &str, default_value: &str) -> String {
  return match env::var_os(var_name) {
    Some(val) => val,
    None => default_value.into()
  }.into_string().unwrap();
}

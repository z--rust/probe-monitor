extern crate cronjob;
use cronjob::CronJob;

fn main() {
    let mut cron = CronJob::new("Test Cron Threaded", on_cron);
    cron.seconds("3");
    CronJob::start_job_threaded(cron);
}

fn on_cron(name: &str) {
  println!("{}: It's time to check health!", name);
}

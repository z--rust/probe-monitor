# Probe monitor

(my first Rust application bundle)

## Preface

Probe worker is collecting some sort of measurements (any kind of non-static data) and dumps the report to server via API every 5 minutes provided that server is accessible (connectivity is checked every 15 seconds). Being unable to send report, the worker saves it for later (retries posting data on connection restore).

Server provides API (methods to authorise, post data and check health) and stores communication and measurement data to the DB. Not being connected to by worker raises alert (log).

## Secure communication

To make sure the massage integrity was not accessed by the 3rd party nor altered, the system should use secure means of communication.

### [JWT with Ed25519](https://docs.rs/jwt-simple)

TODO

# Build everything

```
  cargo build
```

# Run apps

```
  cd ./target/debug
  API_PORT=8090 ./manager
  API_ENDPOINT_URL=localhost:8090 ./worker
```

# TODO list
- cron_task
  - [ ] read arguments and start appropriate task (pass config)
  - [ ] test
- tasks
  - `manager_warn_idle_worker`
    - [ ] reset timeout (restart task) on report received
    - [ ] save log entry on timeout reached
  - `worker_check_manager_health`
    - [ ] start `worker_post_news_to_manager` task (first time)
    - [ ] get response from healthcheck endpoint
      - [ ] check status code
        - [ ] if not 200: cancel `worker_post_news_to_manager` task, create temporary send.lock file with timestamp
        - [ ] if 200: remove send.lock file and start `worker_post_news_to_manager` task
  - `worker_gather_news_data`
    - [ ] gather data for reporting
    - [ ] save report data to sqlite
  - `worker_post_news_to_manager`
    - [ ] send report to manager's API endpoint
- manager (server/API)
  - [x] read ENV and assign variables
  - [ ] create reference implementation in Rust<sup><a name='backlink1' href='#reflink1'>1</a></sup>
  - [ ] start `manager_warn_idle_worker` task
  - listen at endpoints on selected port at return JSON response
    - [ ] `/post` (POST)
      - [ ] timestamp
      - [ ] data
    - [ ] `/healthcheck` (GET)
      - [ ] current timestamp
      - [ ] version
    - [ ] `/dashboard` (GET)
      - [ ] time passed since last report
      - [ ] last N log entries
    - [ ] `/auth` (POST/UPDATE)
      - [ ] get or renew access token
    - [ ] save log of interaction and data to DB (Mongo)
  - [ ] recreate the same server application in Python<sup><a name='backlink2' href='#reflink2'>2</a></sup>
- worker (client/probe)
  - [x] read ENV and assign variables
  - [ ] create reference implementation in Rust<sup><a name='backlink3' href='#reflink3'>3</a></sup>
  - [ ] start `worker_check_manager_health` task
  - [ ] start `worker_gather_news_data` task
  - [ ] create simple mobile application<sup><a name='backlink4' href='#reflink4'>4</a></sup>
- [ ] implement encryption and auth...
- [ ] add `compose.yml` + `Containerfile` files where appropriate (DB and Rust compilation)
- [ ] gitlab ci file to run tests and build artifacts (executables for Linux and Windows)
- [ ] handle exceptions
- [ ] write tests
- [ ] make proper documentation
- [ ] cleanup

<br/>

---
<a name='reflink1' href='#backlink1'>1</a>. Rust CLI server application as primary implementation

<a name='reflink2' href='#backlink2'>2</a>. Django applicaion with interactive dashboard (filters) as secondary implementation

<a name='reflink3' href='#backlink3'>3</a>. Rust CLI applicaion as primary implementation

<a name='reflink4' href='#backlink4'>4</a>. [Flutter mobile applicaion](https://medium.com/flutter-community/using-ffi-on-flutter-plugins-to-run-native-rust-code-d64c0f14f9c2) (configure endpoint, timeouts and report via GUI) as secondary implementation

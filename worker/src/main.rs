#[path = "../../libs/reader.rs"]
mod reader;

#[path = "../libs/news_maker.rs"]
mod news_maker;

//

fn main() {
  let api_endpoint_url = reader::get_env_var("API_ENDPOINT_URL", "http://localhost:8080");
  println!("API endpoint URL: {}", api_endpoint_url);

  let api_healthcheck_interval = reader::get_env_var("API_HC_INTERVAL_SECONDS", "15");
  println!("API healthcheck interval (seconds): {}", api_healthcheck_interval);

  let api_post_interval = reader::get_env_var("API_POST_INTERVAL_MINUTES", "5");
  println!("API post interval (minutes): {}", api_post_interval);

  println!("\nStarting services...");
  // TODO: start cron tasks
}
